from flask import Flask, request
import pymongo
import json

app = Flask(__name__)

db_client = pymongo.MongoClient(host=["172.31.40.115", "172.31.28.147", "172.31.26.128"], port=27017, replicaSet="rs0", connect=True)
print(db_client.test_database)
storage = db_client["chat"]
msg_storage = storage["message"]

@app.route('/messages', methods=['GET'])
def messages():
  limit = request.args.get('limit', type=int)
  msgs = msg_storage.find().limit(limit)
  msgs = [{"text": m['text'], "sender": m['sender'], "time": m['time']} for m in msgs]
  print("Return messages with limit {} - {}".format(limit, json.dumps(msgs)))
  return json.dumps(msgs), 200


@app.route('/send', methods=['POST'])
def send():
  text = request.args.get('text')
  sender = request.args.get('sender')
  time = request.args.get('time')
  res = msg_storage.insert_one({"sender": sender, "time": time, "text": text})
  return str(res), 200


if __name__ == '__main__':
  app.run('0.0.0.0', 5050)

