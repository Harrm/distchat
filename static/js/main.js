
function addMessage(table, msg) {
	  console.log(msg);
	  let msgRow = table.insertRow();
	  let msgSender = msgRow.insertCell(0);
	  let msgText = msgRow.insertCell(1); 
	  let msgTime = msgRow.insertCell(2);
	  msgText.innerHTML = msg.text;
	  msgTime.innerHTML = msg.time;
	  msgSender.innerHTML = msg.sender;

	  return msgRow;
}

function getMessages(limit) {
	  console.log("getMessages");
	  let xhttp = new XMLHttpRequest();
	  xhttp.onreadystatechange = function () {
		      if (this.readyState === 4 && this.status === 200) {
			            let messageList = document.getElementById("message_list");
			      	    console.log("messages: " + this.responseText);
			            let messages = JSON.parse(this.responseText);

					
			            for (const msg of messages) {
					 addMessage(messageList, msg);
				    }
			          } else {
					  console.log("Error: " + this.status);
				  }
		    };
	  xhttp.open("GET", "http://18.222.174.84:5000/messages?limit=" + limit, true);
	  xhttp.send();
}

function sendMessage(text, sender) {
	  let xhttp = new XMLHttpRequest();
	  xhttp.onreadystatechange = function () {
		      if (this.readyState === 4 && this.status === 200) {
			            console.log("Sent successfully");
			          }
		    };
	  time = new Date().getTime();
	  xhttp.open("POST", "http://18.222.174.84:5000/send?text=" + text.value + "&sender=" + sender.value + "&time=" + time, true);
	  xhttp.send();
	  console.log("Sent a message");
}

document.body.onload = () => {
	  getMessages(10);
};

