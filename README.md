
Status of PRIMARY with all three up:
```
rs0:PRIMARY> rs.status()
{
        "set" : "rs0",
        "date" : ISODate("2019-11-02T19:33:15.893Z"),
        "myState" : 1,
        "term" : NumberLong(4),
        "syncingTo" : "",
        "syncSourceHost" : "",
        "syncSourceId" : -1,
        "heartbeatIntervalMillis" : NumberLong(2000),
        "majorityVoteCount" : 2,
        "writeMajorityCount" : 2,
        "optimes" : {
                "lastCommittedOpTime" : {
                        "ts" : Timestamp(1572723195, 1),
                        "t" : NumberLong(4)
                },
                "lastCommittedWallTime" : ISODate("2019-11-02T19:33:15.700Z"),
                "readConcernMajorityOpTime" : {
                        "ts" : Timestamp(1572723195, 1),
                        "t" : NumberLong(4)
                },
                "readConcernMajorityWallTime" : ISODate("2019-11-02T19:33:15.700Z"),
                "appliedOpTime" : {
                        "ts" : Timestamp(1572723195, 1),
                        "t" : NumberLong(4)
                },
                "durableOpTime" : {
                        "ts" : Timestamp(1572723195, 1),
                        "t" : NumberLong(4)
                },
                "lastAppliedWallTime" : ISODate("2019-11-02T19:33:15.700Z"),
                "lastDurableWallTime" : ISODate("2019-11-02T19:33:15.700Z")
        },
        "lastStableRecoveryTimestamp" : Timestamp(1572723175, 1),
        "lastStableCheckpointTimestamp" : Timestamp(1572723175, 1),
        "electionCandidateMetrics" : {
                "lastElectionReason" : "electionTimeout",
                "lastElectionDate" : ISODate("2019-11-02T12:42:04.202Z"),
                "termAtElection" : NumberLong(4),
                "lastCommittedOpTimeAtElection" : {
                        "ts" : Timestamp(0, 0),
                        "t" : NumberLong(-1)
                },
                "lastSeenOpTimeAtElection" : {
                        "ts" : Timestamp(1572698414, 1),
                        "t" : NumberLong(2)
                },
                "numVotesNeeded" : 2,
                "priorityAtElection" : 1,
                "electionTimeoutMillis" : NumberLong(10000),
                "numCatchUpOps" : NumberLong(27017),
                "newTermStartDate" : ISODate("2019-11-02T12:42:05.012Z"),
                "wMajorityWriteAvailabilityDate" : ISODate("2019-11-02T12:42:05.887Z")
        },
        "members" : [
                {
                        "_id" : 0,
                        "name" : "chat1:27017",
                        "ip" : "172.31.40.115",
                        "health" : 1,
                        "state" : 2,
                        "stateStr" : "SECONDARY",
                        "uptime" : 24689,
                        "optime" : {
                                "ts" : Timestamp(1572723195, 1),
                                "t" : NumberLong(4)
                        },
                        "optimeDurable" : {
                                "ts" : Timestamp(1572723195, 1),
                                "t" : NumberLong(4)
                        },
                        "optimeDate" : ISODate("2019-11-02T19:33:15Z"),
                        "optimeDurableDate" : ISODate("2019-11-02T19:33:15Z"),
                        "lastHeartbeat" : ISODate("2019-11-02T19:33:15.772Z"),
                        "lastHeartbeatRecv" : ISODate("2019-11-02T19:33:14.669Z"),
                        "pingMs" : NumberLong(0),
                        "lastHeartbeatMessage" : "",
                        "syncingTo" : "chat3:27017",
                        "syncSourceHost" : "chat3:27017",
                        "syncSourceId" : 2,
                        "infoMessage" : "",
                        "configVersion" : 1
                },
                {
                        "_id" : 1,
                        "name" : "chat2:27017",
                        "ip" : "172.31.28.147",
                        "health" : 1,
                        "state" : 1,
                        "stateStr" : "PRIMARY",
                        "uptime" : 24691,
                        "optime" : {
                                "ts" : Timestamp(1572723195, 1),
                                "t" : NumberLong(4)
                        },
                        "optimeDate" : ISODate("2019-11-02T19:33:15Z"),
                        "syncingTo" : "",
                        "syncSourceHost" : "",
                        "syncSourceId" : -1,
                        "infoMessage" : "",
                        "electionTime" : Timestamp(1572698524, 1),
                        "electionDate" : ISODate("2019-11-02T12:42:04Z"),
                        "configVersion" : 1,
                        "self" : true,
                        "lastHeartbeatMessage" : ""
                },
                {
                        "_id" : 2,
                        "name" : "chat3:27017",
                        "ip" : "172.31.26.128",
                        "health" : 1,
                        "state" : 2,
                        "stateStr" : "SECONDARY",
                        "uptime" : 24681,
                        "optime" : {
                                "ts" : Timestamp(1572723195, 1),
                                "t" : NumberLong(4)
                        },
                        "optimeDurable" : {
                                "ts" : Timestamp(1572723195, 1),
                                "t" : NumberLong(4)
                        },
                        "optimeDate" : ISODate("2019-11-02T19:33:15Z"),
                        "optimeDurableDate" : ISODate("2019-11-02T19:33:15Z"),
                        "lastHeartbeat" : ISODate("2019-11-02T19:33:15.730Z"),
                        "lastHeartbeatRecv" : ISODate("2019-11-02T19:33:15.730Z"),
                        "pingMs" : NumberLong(0),
                        "lastHeartbeatMessage" : "",
                        "syncingTo" : "chat2:27017",
                        "syncSourceHost" : "chat2:27017",
                        "syncSourceId" : 1,
                        "infoMessage" : "",
                        "configVersion" : 1
                }
        ],
        "ok" : 1,
        "$clusterTime" : {
                "clusterTime" : Timestamp(1572723195, 1),
                "signature" : {
                        "hash" : BinData(0,"AAAAAAAAAAAAAAAAAAAAAAAAAAA="),
                        "keyId" : NumberLong(0)
                }
        },
        "operationTime" : Timestamp(1572723195, 1)
}
```
Config of primary with all three up:
```rs0:PRIMARY> rs.config()
{
        "set" : "rs0",
        "version" : 1,
        "protocolVersion" : NumberLong(1),
        "writeConcernMajorityJournalDefault" : true,
        "members" : [
                {
                        "_id" : 0,
                        "host" : "chat1:27017",
                        "arbiterOnly" : false,
                        "buildIndexes" : true,
                        "hidden" : false,
                        "priority" : 1,
                        "tags" : {

                        },
                        "slaveDelay" : NumberLong(0),
                        "votes" : 1
                },
                {
                        "_id" : 1,
                        "host" : "chat2:27017",
                        "arbiterOnly" : false,
                        "buildIndexes" : true,
                        "hidden" : false,
                        "priority" : 1,
                        "tags" : {

                        },
                        "slaveDelay" : NumberLong(0),
                        "votes" : 1
                },
                {
                        "_id" : 2,
                        "host" : "chat3:27017",
                        "arbiterOnly" : false,
                        "buildIndexes" : true,
                        "hidden" : false,
                        "priority" : 1,
                        "tags" : {

                        },
                        "slaveDelay" : NumberLong(0),
                        "votes" : 1
                }
        ],
        "settings" : {
                "chainingAllowed" : true,
                "heartbeatIntervalMillis" : 2000,
                "heartbeatTimeoutSecs" : 10,
                "electionTimeoutMillis" : 10000,
                "catchUpTimeoutMillis" : -1,
                "catchUpTakeoverDelayMillis" : 30000,
                "getLastErrorModes" : {

                },
                "getLastErrorDefaults" : {
                        "w" : 1,
                        "wtimeout" : 0
                },
                "replicaSetId" : ObjectId("5dbcb3c371abb54b56210a80")
        }
}
```
App before shutdown of a node:
![App before](app_before.png)

Status of SECONDARY after PRIMARY was shut:
```
rs0:SECONDARY> rs.status()
{
        "set" : "rs0",
        "date" : ISODate("2019-11-02T19:38:03.749Z"),
        "myState" : 2,
        "term" : NumberLong(5),
        "syncingTo" : "chat1:27017",
        "syncSourceHost" : "chat1:27017",
        "syncSourceId" : 0,
        "heartbeatIntervalMillis" : NumberLong(2000),
        "majorityVoteCount" : 2,
        "writeMajorityCount" : 2,
        "optimes" : {
                "lastCommittedOpTime" : {
                        "ts" : Timestamp(1572723474, 1),
                        "t" : NumberLong(5)
                },
                "lastCommittedWallTime" : ISODate("2019-11-02T19:37:54.715Z"),
                "readConcernMajorityOpTime" : {
                        "ts" : Timestamp(1572723474, 1),
                        "t" : NumberLong(5)
                },
                "readConcernMajorityWallTime" : ISODate("2019-11-02T19:37:54.715Z"),
                "appliedOpTime" : {
                        "ts" : Timestamp(1572723474, 1),
                        "t" : NumberLong(5)
                },
                "durableOpTime" : {
                        "ts" : Timestamp(1572723474, 1),
                        "t" : NumberLong(5)
                },
                "lastAppliedWallTime" : ISODate("2019-11-02T19:37:54.715Z"),
                "lastDurableWallTime" : ISODate("2019-11-02T19:37:54.715Z")
        },
        "lastStableRecoveryTimestamp" : Timestamp(1572723425, 1),
        "lastStableCheckpointTimestamp" : Timestamp(1572723425, 1),
        "members" : [
                {
                        "_id" : 0,
                        "name" : "chat1:27017",
                        "ip" : "172.31.40.115",
                        "health" : 1,
                        "state" : 1,
                        "stateStr" : "PRIMARY",
                        "uptime" : 24969,
                        "optime" : {
                                "ts" : Timestamp(1572723474, 1),
                                "t" : NumberLong(5)
                        },
                        "optimeDurable" : {
                                "ts" : Timestamp(1572723474, 1),
                                "t" : NumberLong(5)
                        },
                        "optimeDate" : ISODate("2019-11-02T19:37:54Z"),
                        "optimeDurableDate" : ISODate("2019-11-02T19:37:54Z"),
                        "lastHeartbeat" : ISODate("2019-11-02T19:38:01.884Z"),
                        "lastHeartbeatRecv" : ISODate("2019-11-02T19:38:03.110Z"),
                        "pingMs" : NumberLong(0),
                        "lastHeartbeatMessage" : "",
                        "syncingTo" : "",
                        "syncSourceHost" : "",
                        "syncSourceId" : -1,
                        "infoMessage" : "",
                        "electionTime" : Timestamp(1572723433, 1),
                        "electionDate" : ISODate("2019-11-02T19:37:13Z"),
                        "configVersion" : 1
                },
                {
                        "_id" : 1,
                        "name" : "chat2:27017",
                        "ip" : "172.31.28.147",
                        "health" : 0,
                        "state" : 8,
                        "stateStr" : "(not reachable/healthy)",
                        "uptime" : 0,
                        "optime" : {
                                "ts" : Timestamp(0, 0),
                                "t" : NumberLong(-1)
                        },
                        "optimeDurable" : {
                                "ts" : Timestamp(0, 0),
                                "t" : NumberLong(-1)
                        },
                        "optimeDate" : ISODate("1970-01-01T00:00:00Z"),
                        "optimeDurableDate" : ISODate("1970-01-01T00:00:00Z"),
                        "lastHeartbeat" : ISODate("2019-11-02T19:38:00.861Z"),
                        "lastHeartbeatRecv" : ISODate("2019-11-02T19:37:13.733Z"),
                        "pingMs" : NumberLong(0),
                        "lastHeartbeatMessage" : "Couldn't get a connection within the time limit",
                        "syncingTo" : "",
                        "syncSourceHost" : "",
                        "syncSourceId" : -1,
                        "infoMessage" : "",
                        "configVersion" : -1
                },
                {
                        "_id" : 2,
                        "name" : "chat3:27017",
                        "ip" : "172.31.26.128",
                        "health" : 1,
                        "state" : 2,
                        "stateStr" : "SECONDARY",
                        "uptime" : 24971,
                        "optime" : {
                                "ts" : Timestamp(1572723474, 1),
                                "t" : NumberLong(5)
                        },
                        "optimeDate" : ISODate("2019-11-02T19:37:54Z"),
                        "syncingTo" : "chat1:27017",
                        "syncSourceHost" : "chat1:27017",
                        "syncSourceId" : 0,
                        "infoMessage" : "",
                        "configVersion" : 1,
                        "self" : true,
                        "lastHeartbeatMessage" : ""
                }
        ],
        "ok" : 1,
        "$clusterTime" : {
                "clusterTime" : Timestamp(1572723474, 1),
                "signature" : {
                        "hash" : BinData(0,"AAAAAAAAAAAAAAAAAAAAAAAAAAA="),
                        "keyId" : NumberLong(0)
                }
        },
        "operationTime" : Timestamp(1572723474, 1)
}
```
Its config:
```
rs0:SECONDARY> rs.config()
{
        "set" : "rs0",
        "version" : 1,
        "protocolVersion" : NumberLong(1),
        "writeConcernMajorityJournalDefault" : true,
        "members" : [
                {
                        "_id" : 0,
                        "host" : "chat1:27017",
                        "arbiterOnly" : false,
                        "buildIndexes" : true,
                        "hidden" : false,
                        "priority" : 1,
                        "tags" : {

                        },
                        "slaveDelay" : NumberLong(0),
                        "votes" : 1
                },
                {
                        "_id" : 1,
                        "host" : "chat2:27017",
                        "arbiterOnly" : false,
                        "buildIndexes" : true,
                        "hidden" : false,
                        "priority" : 1,
                        "tags" : {

                        },
                        "slaveDelay" : NumberLong(0),
                        "votes" : 1
                },
                {
                        "_id" : 2,
                        "host" : "chat3:27017",
                        "arbiterOnly" : false,
                        "buildIndexes" : true,
                        "hidden" : false,
                        "priority" : 1,
                        "tags" : {

                        },
                        "slaveDelay" : NumberLong(0),
                        "votes" : 1
                }
        ],
        "settings" : {
                "chainingAllowed" : true,
                "heartbeatIntervalMillis" : 2000,
                "heartbeatTimeoutSecs" : 10,
                "electionTimeoutMillis" : 10000,
                "catchUpTimeoutMillis" : -1,
                "catchUpTakeoverDelayMillis" : 30000,
                "getLastErrorModes" : {

                },
                "getLastErrorDefaults" : {
                        "w" : 1,
                        "wtimeout" : 0
                },
                "replicaSetId" : ObjectId("5dbcb3c371abb54b56210a80")
        }
}

```
Stopping the primary node:
![Stopping](stopping.png)

App after stopping and adding several messages (still working, huh):
App before shutdown of a node:
![App after](app_after.png)
